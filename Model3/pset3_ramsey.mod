var C K Y w r L G;
varexo tauK tauL tauC;

parameters alpha, beta, delta;
alpha = 0.33;
beta = 0.99;
delta = 0.025;

model;
C(+1) = beta*C*((1+tauC)/(1+tauC(+1)))*(1 - delta + (1-tauK(+1))*r(+1));
r = alpha*(K(-1)/L)^(alpha-1);
w = (1-alpha)*(K(-1)/L)^alpha;
C = (1-L)*w*(1-tauL)/(1+tauC);
(1+tauC)*C = - K + (1-delta)*K(-1) + (1-tauL)*w*L + (1-tauK)*r*K(-1);
Y = (K(-1)^alpha)*(L^(1-alpha));
G = tauC*C + tauL*w*L + tauK*r*K(-1);
end;

initval;
K = 10;
C = 2;
L = 0.5;
tauK = 0;
tauL = 0;
tauC = 0;
end;
steady;

endval;
K = 10;
C = 2;
L = 0.5;
tauK = 0;
tauL = -3;
tauC = 3;
end;
steady;

simul(periods = 200);
