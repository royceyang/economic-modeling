var C K Y G;
varexo tauC;

parameters alpha, beta, delta, A;
alpha = 0.33;
beta = 0.99;
delta = 0.025;
A = 1;

model;
C(+1) = beta*((1+tauC)/(1+tauC(+1)))*C*(1 - delta + alpha*A*K^(alpha-1));
Y = A*K(-1)^alpha;
Y = C + K - (1-delta)*K(-1) + G;
G = tauC*C;
end;

initval;
K = 1;
C = 1;
tauC = 0;
end;
steady;

endval;
K = 1;
C = 1;
tauC = 0.142;
end;
steady;

simul(periods = 200);
