var C K Y w r L G;
varexo tauK tauL;

parameters alpha, beta, delta;
alpha = 0.33;
beta = 0.99;
delta = 0.025;

model;
C(+1) = beta*C*(1 - delta + (1-tauK)*r(+1));
C / (1 - L) = (1 - tauL) * w;
C = - K + (1-delta)*K(-1) + (1-tauL)*w*L + (1-tauK)*r*K(-1);
Y = (K(-1)^alpha)*(L^(1-alpha));
r = alpha*(K(-1)/L)^(alpha-1);
w = (1-alpha)*(K(-1)/L)^alpha;
G = tauL*w*L + tauK*r*K(-1);
end;

initval;
K = 10;
C = 1;
L = 0.5;
tauK = 0;
tauL = 0.28;
end;
steady;

endval;
K = 10;
C = 1;
L = 0.5;
tauK = 0;
tauL = 0.25;
end;
steady;

simul(periods = 200);
