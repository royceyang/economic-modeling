function [residual, g1, g2, g3] = model_tax_KL_dynamic(y, x, params, steady_state, it_)
%
% Status : Computes dynamic model for Dynare
%
% Inputs :
%   y         [#dynamic variables by 1] double    vector of endogenous variables in the order stored
%                                                 in M_.lead_lag_incidence; see the Manual
%   x         [nperiods by M_.exo_nbr] double     matrix of exogenous variables (in declaration order)
%                                                 for all simulation periods
%   steady_state  [M_.endo_nbr by 1] double       vector of steady state values
%   params    [M_.param_nbr by 1] double          vector of parameter values in declaration order
%   it_       scalar double                       time period for exogenous variables for which to evaluate the model
%
% Outputs:
%   residual  [M_.endo_nbr by 1] double    vector of residuals of the dynamic model equations in order of 
%                                          declaration of the equations.
%                                          Dynare may prepend auxiliary equations, see M_.aux_vars
%   g1        [M_.endo_nbr by #dynamic variables] double    Jacobian matrix of the dynamic model equations;
%                                                           rows: equations in order of declaration
%                                                           columns: variables in order stored in M_.lead_lag_incidence followed by the ones in M_.exo_names
%   g2        [M_.endo_nbr by (#dynamic variables)^2] double   Hessian matrix of the dynamic model equations;
%                                                              rows: equations in order of declaration
%                                                              columns: variables in order stored in M_.lead_lag_incidence followed by the ones in M_.exo_names
%   g3        [M_.endo_nbr by (#dynamic variables)^3] double   Third order derivative matrix of the dynamic model equations;
%                                                              rows: equations in order of declaration
%                                                              columns: variables in order stored in M_.lead_lag_incidence followed by the ones in M_.exo_names
%
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

%
% Model equations
%

residual = zeros(7, 1);
T43 = y(1)^params(1);
T45 = y(7)^(1-params(1));
T48 = y(1)/y(7);
lhs =y(9);
rhs =params(2)*y(2)*(1-params(3)+(1-x(it_, 1))*y(10));
residual(1)= lhs-rhs;
lhs =y(2)/(1-y(7));
rhs =(1-x(it_, 2))*y(5);
residual(2)= lhs-rhs;
lhs =y(2);
rhs =(-y(3))+(1-params(3))*y(1)+y(7)*(1-x(it_, 2))*y(5)+y(1)*(1-x(it_, 1))*y(6);
residual(3)= lhs-rhs;
lhs =y(4);
rhs =T43*T45;
residual(4)= lhs-rhs;
lhs =y(6);
rhs =params(1)*T48^(params(1)-1);
residual(5)= lhs-rhs;
lhs =y(5);
rhs =(1-params(1))*T48^params(1);
residual(6)= lhs-rhs;
lhs =y(8);
rhs =y(7)*x(it_, 2)*y(5)+y(1)*x(it_, 1)*y(6);
residual(7)= lhs-rhs;
if nargout >= 2,
  g1 = zeros(7, 12);

  %
  % Jacobian matrix
  %

T72 = getPowerDeriv(T48,params(1)-1,1);
T76 = getPowerDeriv(T48,params(1),1);
  g1(1,2)=(-(params(2)*(1-params(3)+(1-x(it_, 1))*y(10))));
  g1(1,9)=1;
  g1(1,10)=(-(params(2)*y(2)*(1-x(it_, 1))));
  g1(1,11)=(-(params(2)*y(2)*(-y(10))));
  g1(2,2)=1/(1-y(7));
  g1(2,5)=(-(1-x(it_, 2)));
  g1(2,7)=y(2)/((1-y(7))*(1-y(7)));
  g1(2,12)=y(5);
  g1(3,2)=1;
  g1(3,1)=(-(1-params(3)+(1-x(it_, 1))*y(6)));
  g1(3,3)=1;
  g1(3,5)=(-(y(7)*(1-x(it_, 2))));
  g1(3,6)=(-((1-x(it_, 1))*y(1)));
  g1(3,7)=(-((1-x(it_, 2))*y(5)));
  g1(3,11)=(-(y(1)*(-y(6))));
  g1(3,12)=(-(y(7)*(-y(5))));
  g1(4,1)=(-(T45*getPowerDeriv(y(1),params(1),1)));
  g1(4,4)=1;
  g1(4,7)=(-(T43*getPowerDeriv(y(7),1-params(1),1)));
  g1(5,1)=(-(params(1)*1/y(7)*T72));
  g1(5,6)=1;
  g1(5,7)=(-(params(1)*T72*(-y(1))/(y(7)*y(7))));
  g1(6,1)=(-((1-params(1))*1/y(7)*T76));
  g1(6,5)=1;
  g1(6,7)=(-((1-params(1))*T76*(-y(1))/(y(7)*y(7))));
  g1(7,1)=(-(x(it_, 1)*y(6)));
  g1(7,5)=(-(y(7)*x(it_, 2)));
  g1(7,6)=(-(x(it_, 1)*y(1)));
  g1(7,7)=(-(x(it_, 2)*y(5)));
  g1(7,8)=1;
  g1(7,11)=(-(y(1)*y(6)));
  g1(7,12)=(-(y(7)*y(5)));

if nargout >= 3,
  %
  % Hessian matrix
  %

  g2 = sparse([],[],[],7,144);
if nargout >= 4,
  %
  % Third order derivatives
  %

  g3 = sparse([],[],[],7,1728);
end
end
end
end
