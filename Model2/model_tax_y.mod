var C K Y G;
varexo tauY;

parameters alpha, beta, delta, A;
alpha = 0.33;
beta = 0.99;
delta = 0.025;
A = 1;

model;
C(+1) = beta*C*(1 - delta + alpha*(1-tauY(+1))*A*K^(alpha-1));
Y = A*K(-1)^alpha;
Y = C + K - (1-delta)*K(-1) + G;
G = tauY*Y;
end;

initval;
K = 1;
C = 1;
tauY = 0;
end;
steady;

endval;
K = 1;
C = 1;
tauY = 0.1;
end;
steady;

simul(periods = 200);
