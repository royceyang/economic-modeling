function [residual, g1, g2, g3] = model_tax_KL_static(y, x, params)
%
% Status : Computes static model for Dynare
%
% Inputs : 
%   y         [M_.endo_nbr by 1] double    vector of endogenous variables in declaration order
%   x         [M_.exo_nbr by 1] double     vector of exogenous variables in declaration order
%   params    [M_.param_nbr by 1] double   vector of parameter values in declaration order
%
% Outputs:
%   residual  [M_.endo_nbr by 1] double    vector of residuals of the static model equations 
%                                          in order of declaration of the equations.
%                                          Dynare may prepend or append auxiliary equations, see M_.aux_vars
%   g1        [M_.endo_nbr by M_.endo_nbr] double    Jacobian matrix of the static model equations;
%                                                       columns: variables in declaration order
%                                                       rows: equations in order of declaration
%   g2        [M_.endo_nbr by (M_.endo_nbr)^2] double   Hessian matrix of the static model equations;
%                                                       columns: variables in declaration order
%                                                       rows: equations in order of declaration
%   g3        [M_.endo_nbr by (M_.endo_nbr)^3] double   Third derivatives matrix of the static model equations;
%                                                       columns: variables in declaration order
%                                                       rows: equations in order of declaration
%
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

residual = zeros( 7, 1);

%
% Model equations
%

T39 = y(2)^params(1);
T41 = y(6)^(1-params(1));
T44 = y(2)/y(6);
lhs =y(1);
rhs =y(1)*params(2)*(1-params(3)+(1-x(1))*y(5));
residual(1)= lhs-rhs;
lhs =y(1)/(1-y(6));
rhs =(1-x(2))*y(4);
residual(2)= lhs-rhs;
lhs =y(1);
rhs =(-y(2))+(1-params(3))*y(2)+y(6)*(1-x(2))*y(4)+(1-x(1))*y(5)*y(2);
residual(3)= lhs-rhs;
lhs =y(3);
rhs =T39*T41;
residual(4)= lhs-rhs;
lhs =y(5);
rhs =params(1)*T44^(params(1)-1);
residual(5)= lhs-rhs;
lhs =y(4);
rhs =(1-params(1))*T44^params(1);
residual(6)= lhs-rhs;
lhs =y(7);
rhs =y(6)*x(2)*y(4)+y(2)*x(1)*y(5);
residual(7)= lhs-rhs;
if ~isreal(residual)
  residual = real(residual)+imag(residual).^2;
end
if nargout >= 2,
  g1 = zeros(7, 7);

  %
  % Jacobian matrix
  %

T69 = getPowerDeriv(T44,params(1)-1,1);
T73 = getPowerDeriv(T44,params(1),1);
  g1(1,1)=1-params(2)*(1-params(3)+(1-x(1))*y(5));
  g1(1,5)=(-(y(1)*params(2)*(1-x(1))));
  g1(2,1)=1/(1-y(6));
  g1(2,4)=(-(1-x(2)));
  g1(2,6)=y(1)/((1-y(6))*(1-y(6)));
  g1(3,1)=1;
  g1(3,2)=(-((1-x(1))*y(5)+(-1)+1-params(3)));
  g1(3,4)=(-(y(6)*(1-x(2))));
  g1(3,5)=(-((1-x(1))*y(2)));
  g1(3,6)=(-((1-x(2))*y(4)));
  g1(4,2)=(-(T41*getPowerDeriv(y(2),params(1),1)));
  g1(4,3)=1;
  g1(4,6)=(-(T39*getPowerDeriv(y(6),1-params(1),1)));
  g1(5,2)=(-(params(1)*1/y(6)*T69));
  g1(5,5)=1;
  g1(5,6)=(-(params(1)*T69*(-y(2))/(y(6)*y(6))));
  g1(6,2)=(-((1-params(1))*1/y(6)*T73));
  g1(6,4)=1;
  g1(6,6)=(-((1-params(1))*T73*(-y(2))/(y(6)*y(6))));
  g1(7,2)=(-(x(1)*y(5)));
  g1(7,4)=(-(y(6)*x(2)));
  g1(7,5)=(-(x(1)*y(2)));
  g1(7,6)=(-(x(2)*y(4)));
  g1(7,7)=1;
  if ~isreal(g1)
    g1 = real(g1)+2*imag(g1);
  end
if nargout >= 3,
  %
  % Hessian matrix
  %

  g2 = sparse([],[],[],7,49);
if nargout >= 4,
  %
  % Third order derivatives
  %

  g3 = sparse([],[],[],7,343);
end
end
end
end
